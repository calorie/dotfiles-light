" startuptime
if has('vim_starting') && has('reltime')
  let g:startuptime = reltime()
  augroup vimrc-startuptime
    autocmd! VimEnter * let g:startuptime = reltime(g:startuptime) | redraw
    \ | echomsg 'startuptime: ' . reltimestr(g:startuptime)
  augroup END
endif

" plugin
source ~/dotfiles/.vimrc.bundle
" basic
source ~/dotfiles/.vimrc.basic
" statusLine
source ~/dotfiles/.vimrc.statusline
" indent
source ~/dotfiles/.vimrc.indent
" apperance
source ~/dotfiles/.vimrc.apperance
" completion
source ~/dotfiles/.vimrc.completion
" tags
source ~/dotfiles/.vimrc.tags
" search
source ~/dotfiles/.vimrc.search
" move
source ~/dotfiles/.vimrc.moving
" color
source ~/dotfiles/.vimrc.colors
" edit
source ~/dotfiles/.vimrc.editing
" encoding
source ~/dotfiles/.vimrc.encoding
" plugin's setting
source ~/dotfiles/.vimrc.plugins_setting

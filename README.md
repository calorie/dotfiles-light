dotfiles
========
## requirements

- vim
- git

## setup

```
$ git clone https://bitbucket.org/calorie/dotfiles-light.git ~/dotfiles
$ cd ~/dotfiles
$ ./setup.sh
```

## uninstall

```
$ cd ~/dotfiles
$ ./uninstall.sh
```


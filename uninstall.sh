#!/bin/bash
while :
do
  read -p "uninstall dotfiles? y/n " yn
  if [ $yn = "n" -o $yn = "N" ]; then
    exit
  elif [ $yn = "y" -o $yn = "Y" ]; then
    break
  else
    continue
  fi
done

DOT_FILES=(
  .zshrc .ctags .gdbinit
  .gemrc .gitconfig .gitignore
  .inputrc .irbrc .vimrc
  .vrapperrc .dir_colors
  .rdebugrc .vim dotfiles )

for file in ${DOT_FILES[@]}
do
  rm -rf $HOME/$file
done

exec $SHELL

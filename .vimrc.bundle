"---------------------------------------------------------------------
" neobunlde.vim
"---------------------------------------------------------------------

set nocompatible
filetype plugin indent off
if has('vim_starting')
  set runtimepath+=~/.vim/bundle/neobundle.vim
endif
call neobundle#rc(expand('~/.vim/bundle/'))

" let g:neobundle#types#git#default_protocol = 'https'

NeoBundleFetch 'Shougo/neobundle.vim'

" Unite {{{

  " unite
  NeoBundleLazy 'Shougo/unite.vim', {
        \ 'autoload' : {
        \   'commands' : [
        \     'Unite', 'UniteWithCurrentDir',
        \     'UniteWithBufferDir', 'UniteBookmarkAdd',
        \     'UniteWithCursorWord'],
        \ }}

" }}}

" Edit {{{

  " yank history
  NeoBundle 'YankRing.vim'

  "comment
  NeoBundleLazy 'tomtom/tcomment_vim', {
        \ 'autoload' : {
        \   'commands' : ['TComment',
        \     'TCommentAs', 'TCommentMaybeInline']
        \ }}

  " format text
  NeoBundleLazy 'godlygeek/tabular', {
        \ 'autoload' : { 'commands' : 'Tab' }}

  " surround text
  NeoBundleLazy 'tpope/vim-surround', {
        \ 'autoload' : {
        \   'mappings' : [
        \     ['nx', '<Plug>Dsurround'], ['nx', '<Plug>Csurround' ],
        \     ['nx', '<Plug>Ysurround' ], ['nx', '<Plug>YSurround' ],
        \     ['nx', '<Plug>Yssurround'], ['nx', '<Plug>YSsurround'],
        \     ['nx', '<Plug>YSsurround'], ['nx', '<Plug>VgSurround'],
        \     ['nx', '<Plug>VSurround']]
        \ }}

" }}}

" Completion {{{

  NeoBundleLazy 'Shougo/neocomplcache', {
        \ 'autoload' : { 'insert' : 1 }}

  " neocomplcache's snippet
  NeoBundleLazy 'Shougo/neosnippet', {
        \ 'autoload' : {
        \   'commands' : ['NeoSnippetEdit'],
        \   'filetypes' : 'snippet',
        \   'insert' : 1,
        \   'unite_sources' : ['snippet', 'neosnippet/user',
        \     'neosnippet/runtime'],
        \ }}

" }}}

" Searching/Moving {{{

  " move cursol by <Leader>w/f
  NeoBundle 'EasyMotion'

  " expand %
  NeoBundleLazy 'edsono/vim-matchit', { 'autoload' : {
        \ 'mappings' : ['nx', '%'] }}

  " move text when visual
  NeoBundleLazy 't9md/vim-textmanip', { 'autoload' : {
        \ 'mappings' : [
        \   ['x', '<Plug>(textmanip-move-down)'],
        \   ['x', '<Plug>(textmanip-move-up)'],
        \   ['x', '<Plug>(textmanip-move-left)' ],
        \   ['x', '<Plug>(textmanip-move-right)']],
        \ }}

  " recursive f move
  NeoBundleLazy 'rhysd/clever-f.vim', { 'autoload' : {
        \ 'mappings' : [
        \   '<Plug>(clever-f-f)', '<Plug>(clever-f-F)'],
        \ }}

  " window swap
  NeoBundleLazy 'calorie/vim-swap-windows', { 'autoload' : {
        \ 'commands' : [
        \   'MarkWindow', 'SwapWindows'],
        \ }}

" }}}

" Programming {{{

  NeoBundle 'szw/vim-tags'

  " quick compile
  NeoBundleLazy 'thinca/vim-quickrun', {
        \ 'autoload' : {
        \   'mappings' : [['nxo', '<Plug>(quickrun)']],
        \   'commands' : 'QuickRun',
        \ }}

  " html emmet-vim
  NeoBundleLazy 'mattn/emmet-vim', {
        \ 'autoload': {
        \   'filetypes': ['html', 'eruby', 'php',
        \     'haml', 'css', 'scss', 'sass'],
        \ }}

" }}}

" Syntax {{{

  " syntax checking plugins exist for
  " eruby, haml, html, javascript, php, python, ruby and sass.
  NeoBundleLazy 'scrooloose/syntastic', {
        \ 'build' : {
        \   'mac' : ['brew install tidy', 'brew install csslint',
        \            'gem install sass', 'brew install jslint'] },
        \ 'autoload': {
        \   'insert' : 1,
        \ }}

" }}}

" Buffer {{{

  " tree exproler
  NeoBundle 'The-NERD-tree', {
        \ 'autoload' : {
        \   'commands' : ['NERDTreeToggle','NERDTreeFind','NERDTree']
        \ }}

" }}}


" Utility {{{

  " async execution
  NeoBundle 'Shougo/vimproc', {
        \ 'build' : {
        \   'mac'    : 'make -f make_mac.mak',
        \   'unix'   : 'make -f make_unix.mak',
        \   'cygwin' : 'make -f make_cygwin.mak',
        \ }}

  " cool statusline
  NeoBundle 'bling/vim-airline'

  " async execution
  NeoBundleLazy 'tpope/vim-dispatch', {
        \ 'autoload': { 'commands': ['Dispatch', 'Make', 'Start'] }
        \ }

  " sudo vim
  NeoBundleLazy 'vim-scripts/sudo.vim', {
        \ 'autoload': { 'commands': ['SudoRead', 'SudoWrite'] }
        \ }

" }}}

" ColorScheme {{{

  " jellybeans, wombat
  NeoBundle 'calorie/colorschemes'

" }}}

filetype plugin indent on

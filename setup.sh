#!/bin/bash
while :
do
  read -p "install dotfiles? y/n " yn
  if [ $yn = "n" -o $yn = "N" ]; then
    exit
  elif [ $yn = "y" -o $yn = "Y" ]; then
    break
  else
    continue
  fi
done

ORIGIN=$(pwd)
DOTFILES_PATH=$(cd $(dirname $0); pwd)

[ -d $HOME/.vim/bundle ] || mkdir -p $HOME/.vim/bundle

# symbolic link
DOTFILES=(
  .zshrc .ctags .gdbinit
  .gemrc .gitconfig .gitignore
  .inputrc .irbrc .vimrc .vrapperrc
  .dir_colors .rdebugrc .vim/dict )
for file in ${DOTFILES[@]}
do
  ln -sb $DOTFILES_PATH/$file $HOME/$file
done

# NeoBundle
cd $HOME/.vim/bundle
git clone https://github.com/Shougo/neobundle.vim.git
yes '' | vim +NeoBundleInstall +qa

cd ${ORIGIN}
exec $SHELL
